//
//  ViewController.swift
//  bank
//
//  Created by ِAhmed bahaa on 12/25/17.
//  Copyright © 2017 ِAhmed bahaa. All rights reserved.
//

import UIKit



class ViewController: UIViewController  ,UICollectionViewDataSource ,UICollectionViewDelegate  {
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var myCollectionView: UICollectionView!
    let array:[String] = ["1","2","3","4","5","6","7","8","9","10","1","2","3","4","5","6","7","8","9","10","1","2","3","4","5","6","7","8","9","10"]
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        let tabPageViewController = UIPageViewController.prepare()
        let vc1 = UIViewController()
        let vc2 = UIViewController()
        
        tabPageViewController.tabItems = [(vc1, "First"), (vc2, "Second")]
        
    //    TabPageOption.currentColor = UIColor.redColor()
        // Do any additional setup after loading the view, typically from a nib
        let itemSize = UIScreen.main.bounds.width/2-2
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsetsMake(20, 0, 10, 0)
        layout.itemSize = CGSize(width: itemSize, height: itemSize)
        layout.minimumInteritemSpacing = 2
        layout.minimumLineSpacing = 2
        myCollectionView.collectionViewLayout = layout
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //number of views
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array.count
        
    }
    //populate views
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath )as! myPhotoCollectionViewCell
        cell.myPhotoView.image = UIImage(named: array[indexPath.row] + ".jpeg")
        return cell 
    }

   
}

